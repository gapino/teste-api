﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiRest.Services
{
    public interface IInsertRouteFile
    {
        Task<bool> InsertRoute<T>(string path, T obj) where T : IFormFile;
    }
}
