﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiRest.Services
{
    public interface IServiceLibrary
    {
        string BestRoute(string path, string start, string end);
    }
}
