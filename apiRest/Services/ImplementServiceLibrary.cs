﻿using ShortWayLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiRest.Services
{
    public class ImplementServiceLibrary : IServiceLibrary
    {

        public ImplementServiceLibrary()
        {

        }
        public string BestRoute(string path, string start, string end)
        {
            try
            {
                FileHelper fileHelper = new FileHelper(path);

                var result = fileHelper.ExistArquivo();

                if (!result.IsSuccess)
                {
                    throw new Exception(result.Message);
                }

                UtilLibrary utilLibrary = new UtilLibrary(fileHelper.GetLinesFile());

                utilLibrary.DoNodes();
                var route = utilLibrary.PrintResult(start, end);
                return route;
            }
            catch (Exception msg)
            {
                return msg.Message;
            }
        }
    }
}
