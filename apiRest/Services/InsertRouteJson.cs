﻿using apiRest.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace apiRest.Services
{
    public class InsertRouteJson : IInsertRouteJson
    {
        public async  Task<bool> InsertRoute<T>(string path, T obj) where T : Route
        {
			try
			{
                FileInfo fileInfo = new FileInfo(path);
                var fileLocal = fileInfo.AppendText();
                await fileLocal.WriteLineAsync($"\n{obj.Source},{obj.Target},{obj.Distance}");
                fileLocal.Close();
                return true;
            }
			catch (Exception)
			{

				throw;
			}
        }
    }
}
