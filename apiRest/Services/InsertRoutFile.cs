﻿using apiRest.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace apiRest.Services
{
    public class InsertRoutFile : IInsertRouteFile
    {
        public async Task<bool> InsertRoute<T>(string path, T obj) where T : IFormFile
        {
            try
            {
                var filePath = Path.Combine(path);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    await obj.CopyToAsync(fileStream);
                }
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
