﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using apiRest.Models;
using apiRest.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace apiRest.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class RoutesController : ControllerBase
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IServiceLibrary _serviceLibrary;
        private readonly IInsertRouteJson _insertRouteJson;
        private readonly IInsertRouteFile _insertRouteFile;

        public RoutesController(IHostingEnvironment environment,
            IServiceLibrary serviceLibrary, IInsertRouteJson insertRouteJson,
            IInsertRouteFile insertRouteFile)
        {
            _hostingEnvironment = environment;
            _serviceLibrary = serviceLibrary;
            _insertRouteJson = insertRouteJson;
            _insertRouteFile = insertRouteFile;
        }

        [HttpGet]
        public IActionResult Get(string start, string end)
        {
            try
            {
                var path = Path.Combine(_hostingEnvironment.WebRootPath, "input-routes.csv");
                var response = _serviceLibrary.BestRoute(path, start, end);
                return Ok(response);
            }
            catch (Exception msg)
            {
                return NotFound(msg.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> PostRoute(Route route)
        {
            try
            {
                if (route != null)
                {
                    if (String.IsNullOrEmpty(route.Source)){
                        return NotFound("Invalid Source");
                    }

                    if (String.IsNullOrEmpty(route.Target))
                    {
                        return NotFound("Invalid Target");
                    }

                    if(route.Distance <= 0)
                    {
                        return NotFound("Invalid distance");
                    }

                    var path = Path.Combine(_hostingEnvironment.WebRootPath, "input-routes.csv");
                    await _insertRouteJson.InsertRoute<Route>(path, route);
                    return StatusCode(201, route);
                }

                return NotFound("Erro");
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPost("upload")]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
            try
            {
                if (file != null && file.Length > 0)
                {
                    var filePath = Path.Combine(_hostingEnvironment.WebRootPath, "input-routes.csv");
                    await _insertRouteFile.InsertRoute<IFormFile>(filePath, file);
                    return Ok("File Upload");
                }

                return NotFound("Erro");
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }
    }
}
