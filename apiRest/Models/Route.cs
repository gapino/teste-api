﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiRest.Models
{
    public class Route
    {
        public string Target { get; set; }

        public string Source { get; set; }

        public int Distance { get; set; }
    }
}
